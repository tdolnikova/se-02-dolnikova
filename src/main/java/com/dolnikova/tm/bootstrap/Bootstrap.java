package com.dolnikova.tm.bootstrap;

import com.dolnikova.tm.repository.ProjectRepository;
import com.dolnikova.tm.repository.TaskRepository;

import java.util.Scanner;

public class Bootstrap {

    private static final String HELP = "help";
    private static final String CREATE_PROJECT = "create project";
    private static final String READ_PROJECT = "read project";
    private static final String UPDATE_PROJECT = "update project";
    private static final String DELETE_PROJECT = "delete project";
    private static final String CREATE_TASK = "create task";
    private static final String READ_TASK = "read task";
    private static final String UPDATE_TASK = "update task";
    private static final String DELETE_TASK = "delete task";
    private static final String EXIT = ">> press enter <<";
    private static final String HELP_STRING = HELP + ": show all commands\n"
            + CREATE_PROJECT + ": create a project\n" + READ_PROJECT + ": read a project\n" + UPDATE_PROJECT + ": update a project\n" + DELETE_PROJECT + ":delete a project\n"
            + CREATE_TASK + ": create a task\n" + READ_TASK + ": read a task\n" + UPDATE_TASK + ": update a task\n" + DELETE_TASK + ": delete a task\n" + EXIT + ": exit";

    public void init() {
        System.out.println("* * * Welcome to Task Manager * * *");
        Scanner scanner = new Scanner(System.in);
        ProjectRepository projectManager = new ProjectRepository();
        TaskRepository taskManager = new TaskRepository();
        String input = "project";
        while (!input.isEmpty()) {
            input = scanner.nextLine();
            switch (input) {
                case HELP:
                    showHelp();
                    break;
                case CREATE_PROJECT:
                    projectManager.createProject();
                    break;
                case READ_PROJECT:
                    projectManager.readProject();
                    break;
                case UPDATE_PROJECT:
                    projectManager.updateProject();
                    break;
                case DELETE_PROJECT:
                    projectManager.deleteProject();
                    break;
                case CREATE_TASK:
                    taskManager.createTask();
                    break;
                case READ_TASK:
                    taskManager.readTask();
                    break;
                case UPDATE_TASK:
                    taskManager.updateTask();
                    break;
                case DELETE_TASK:
                    taskManager.deleteTask();
                    break;
                default:
                    if (!input.isEmpty())
                        System.out.println("Такой команды не существует");
                    break;
            }
        }
    }

    private static void showHelp() {
        System.out.println(HELP_STRING);
    }

}
