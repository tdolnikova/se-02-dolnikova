package com.dolnikova.tm.repository;

import com.dolnikova.tm.entity.Project;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class ProjectRepository {

    private static Scanner scanner = new Scanner(System.in);
    public static List<Project> projects = new ArrayList<>();

    public void createProject() {
        System.out.println("Введите название нового проекта.");
        String projectName = scanner.nextLine();
        if (projectName.isEmpty()) return;
        for (Project project : projects) {
            if (projectName.equals(project.getProjectName())) {
                System.out.println("Проект с таким названием уже существует.");
                return;
            }
        }
        projects.add(new Project(projectName));
        System.out.println("Проект " + projectName + " создан.");
    }

    public void readProject() {
        if (projects.isEmpty()) {
            System.out.println("Проектов нет.");
            return;
        }
        System.out.println("Введите название проекта");
        boolean fileFound = false;
        boolean tasksFound = false;
        boolean projectRead = false;
        while (!projectRead) {
            String projectName = scanner.nextLine();
            if (projectName.isEmpty()) break;
            for (Project project : projects) {
                if (projectName.equals(project.getProjectName())) {
                    fileFound = true;
                    if (project.getTasks().size() != 0) tasksFound = true;
                    for (int i = 0; i < project.getTasks().size(); i++) {
                        System.out.println("#" + (i + 1) + " " + project.getTasks().get(i).getTaskText());
                    }
                    projectRead = true;
                }
            }

            if (!fileFound) System.out.println("Проекта " + projectName + " не существует. Попробуйте еще раз.");
            else if (!tasksFound) {
                System.out.println("В проекте " + projectName + " нет задач");
                projectRead = true;
            } else projectRead = true;
        }
    }

    public void updateProject() {
        System.out.println("Введите название проекта.");
        boolean fileFound = false;
        while (!fileFound) {
            String projectName = scanner.nextLine();
            for (Project project : projects) {
                if (projectName.equals(project.getProjectName())) {
                    fileFound = true;
                    System.out.println("Введите новое название проекта.");
                    while (true) {
                        String newProjectName = scanner.nextLine();
                        if (!newProjectName.isEmpty()) {
                            project.setProjectName(newProjectName);
                            System.out.println("Проект " + newProjectName + " обновлен.");
                            break;
                        }
                    }
                }
            }
            if (!fileFound) System.out.println("Проекта " + projectName + " не существует. Попробуйте еще раз.");
        }
    }

    public void deleteProject() {
        if (projects.isEmpty()) {
            System.out.println("Проектов нет.");
            return;
        }
        System.out.println("Какой проект хотите удалить?");
        Project project = selectProject();
        if (project != null) {
            projects.remove(project);
            System.out.println("Проект удален.");
        }
    }

    private static Project selectProject() {
        String projectName;
        while (true) {
            projectName = scanner.nextLine();
            if (projectName.isEmpty()) return null;
            for (Project project : projects) {
                if (projectName.equals(project.getProjectName())) return project;
            }
            System.out.println("Проекта " + projectName + " не существует. Попробуйте еще раз.");
        }
    }

}
