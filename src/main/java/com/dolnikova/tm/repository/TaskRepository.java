package com.dolnikova.tm.repository;

import com.dolnikova.tm.entity.Project;
import com.dolnikova.tm.entity.Task;

import java.util.Scanner;

import static com.dolnikova.tm.repository.ProjectRepository.projects;

public class TaskRepository {

    private static Scanner scanner = new Scanner(System.in);

    public void createTask() {
        System.out.println("Выберите проект");
        boolean taskCreated = false;
        boolean fileFound = false;
        while (!taskCreated) {
            String projectName = scanner.nextLine();
            for (Project project : projects) {
                if (projectName.equals(project.getProjectName())) {
                    fileFound = true;
                    System.out.println("Введите задачу: ");
                    while(!taskCreated) {
                        String taskText = scanner.nextLine();
                        if (taskText.isEmpty()) taskCreated = true;
                        else {
                            project.addTask(new Task(taskText));
                            System.out.println("Задача " + taskText + " добавлена.");
                        }
                    }
                }
            }
            if (!fileFound) {
                System.out.println("Проекта с таким названием не существует.");
                taskCreated = true;
            }
        }
    }

    public void readTask() {
        System.out.println("Введите название проекта.");
        Project project = selectProject();
        if (project != null) {
            System.out.println("В проекте " + project.getProjectName() + " " + project.getTasks().size() + " задач. Введите номер задачи.");
            boolean taskRead = false;
            while (!taskRead) {
                String taskNumber = scanner.nextLine();
                if (taskNumber.isEmpty()) break;
                if (isNumber(taskNumber)) {
                    int num = Integer.parseInt(taskNumber);
                    if (taskExists(num, project.getTasks().size())) {
                        System.out.println("#" + num + " " + project.readTask(num).getTaskText());
                        taskRead = true;
                    }
                    else System.out.println("Некорректный ввод числа.");
                } else
                    System.out.println("Некорректный ввод числа.");
            }
        }
    }

    private static boolean isNumber(String strNum) {
        try {
            Integer.parseInt(strNum);
        } catch (NumberFormatException | NullPointerException nfe) {
            return false;
        }
        return true;
    }

    public void updateTask() {
        System.out.println("Введите название проекта.");
        Project project = selectProject();
        if (project != null) {
            int taskNumber = project.getTasks().size();
            if (taskNumber == 0) {
                System.out.println("В проекте нет задач");
                return;
            }
            System.out.println("В проекте " + project.getProjectName() + " "
                    + taskNumber + " задач. Введите номер задачи для редактирования.");
            boolean taskUpdated = false;
            while (!taskUpdated) {
                String number = scanner.nextLine();
                if (number.isEmpty()) break;
                if (isNumber(number)) {
                    int num = Integer.parseInt(number);
                    if (taskExists(num, taskNumber)) {
                        System.out.println("Введите новую задачу");
                        String newTaskText = scanner.nextLine();
                        if (newTaskText.isEmpty()) break;
                        project.updateTask(num, new Task(newTaskText));
                        System.out.println("Задача изменена.");
                        taskUpdated = true;
                    } else
                        System.out.println("Попробуйте ввести число еще раз");
                } else System.out.println("Попробуйте ввести число еще раз");
            }
        }
    }

    public void deleteTask() {
        System.out.println("Введите название проекта.");
        Project project = selectProject();
        if (project != null) {
            int taskNumber = project.getTasks().size();
            System.out.println("В проекте " + taskNumber + " задач. Введите номер задачи для удаления.");
            boolean taskDeleted = false;
            while (!taskDeleted) {
                String number = scanner.nextLine();
                if (number.isEmpty()) break;
                if (isNumber(number)) {
                    int num = Integer.parseInt(number);
                    if (taskExists(num, taskNumber)) {
                        project.deleteTask(num);
                        System.out.println("Задача удалена.");
                        taskDeleted = true;
                    }
                }
            }
        }
    }

    private static boolean taskExists(int input, int projectSize) {
        return input > 0 && input <= projectSize;
    }

    private static Project selectProject() {
        String projectName;
        while (true) {
            projectName = scanner.nextLine();
            if (projectName.isEmpty()) return null;
            for (Project project : projects) {
                if (projectName.equals(project.getProjectName())) return project;
            }
            System.out.println("Проекта " + projectName + " не существует. Попробуйте еще раз.");
        }
    }

}
